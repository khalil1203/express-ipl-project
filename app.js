const express = require('express');
const path = require('path');
const { PORT } = require('./config.js');
const requestId = require('express-request-id');
const fs = require('fs');
const app = express();
const router = express.Router();

const answer1 = require('./src/server/1-matches-per-year.cjs');
const answer2 = require('./src/server/2-matches-won-per-team-per-year.cjs');
const answer3 = require('./src/server/3-extra-runs-per-team-2016.cjs');
const answer4 = require('./src/server/4-top-10-economical-bowlers-in-the-year-2015.cjs');
const answer5 = require('./src/server/5-number-of-times-team-won-toss-and-match.cjs');
const answer6 = require('./src/server/6-player-with-highest-Player-of-the-Match-for-season.cjs');
const answer7 = require('./src/server/7-strike-rate-of-a-batsman-for-each-season.cjs');
const answer8 = require('./src/server/8-highest-times-player-dismissed-by-another-player.cjs');
const answer9 = require('./src/server/9-bowler-with-best-economy-in-super-overs.cjs');


app.use(requestId());


app.use((req, res, next) => {
  console.log(req.url);
  let url = req.url;
  if (url != "/favicon.ico") {
    const content = url + "          ,    Request Id :  " + req.id + "         , Time : " + new Date() + '\n';
    fs.appendFile('./log.log', content, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
  next();

});


app.use(router);


router.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, "views", "homepage.html"));
});



router.get('/answer1', answer1);
router.get('/answer2', answer2);
router.get('/answer3', answer3);
router.get('/answer4', answer4);
router.get('/answer5', answer5);
router.get('/answer6', answer6);
router.get('/answer7', answer7);
router.get('/answer8', answer8);
router.get('/answer9', answer9);

router.get('/log', (req, res) => {
  res.sendFile(path.join(__dirname, "log.log"));
});


app.use(function (err, req, res, next) {

  res.status(500);
  res.status(400).json(err);
});


app.use(function (req, res, next) {

  res.status(500);
  res.sendFile(path.join(__dirname, "views", "error.html"));
});


app.listen(9000, (err) => {
  if (err) {
    console.log(err);
  }
  else {
    console.log(`server is running on : ${PORT}`);
  }

})

module.exports = app;
