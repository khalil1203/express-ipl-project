const deliveriesPath = 'src/data/deliveries.csv';
const fs = require('fs');
const csvToJson = require('csvtojson');


const answer9 = (req, res, next) => {

    const bestPlayer_Arr = [];

    csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveries) => {

            deliveries.map((entries) => {
                if (entries.is_super_over != '0') {
                    let found = false;

                    bestPlayer_Arr.map((data) => {
                        if (entries.bowler == data.bowler) {


                            data.runs += parseInt(entries.total_runs);
                            data.ball += 1;

                            let tempEconomy = (data.runs * 6) / data.ball;
                            data.economy = tempEconomy;
                            found = true;
                        }
                    });

                    if (!found) {
                        let obj = {
                            bowler: entries.bowler,
                            runs: parseInt(entries.total_runs),
                            ball: 1,
                            economy: entries.total_runs * 6
                        }
                        bestPlayer_Arr.push(obj);
                    }
                }

            });

            const answr = bestPlayer_Arr.sort((prev1, cur1) => {
                if (prev1.economy > cur1.economy) {
                    return 1;
                }
                else if (prev1.economy < cur1.economy) {
                    return -1;
                }
                else {
                    return 0;
                }
            }).slice(0, 1);

            res.json(answr);

        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });


}
module.exports = answer9;