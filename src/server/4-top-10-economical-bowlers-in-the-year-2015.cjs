const csvToJson = require('csvtojson');
const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');


const answer4 = (req, res, next) => {



    match_Id_Arr = [];

    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            match_Id_Arr.push(
                ...data
                    .filter((item) => {
                        return item.season === '2015';
                    })
                    .map((item) => {
                        return item.id;
                    })
            );
        });


    csvToJson()
        .fromFile(deliveriesPath)
        .then((data) => {
            const counter = {};
            const ans = {};
            const topTenEconomicalBaller = data.reduce((acc, entries) => {
                const { bowler, total_runs, match_id } = entries;
                if (match_Id_Arr.includes(match_id)) {

                    if (acc[bowler]) {
                        ans[bowler] += parseInt(total_runs);
                        counter[bowler] += 1;
                        acc[bowler] = ans[bowler] * 6 / counter[bowler];
                    }
                    else {
                        ans[bowler] = parseInt(total_runs);
                        counter[bowler] = 1;
                        acc[bowler] = ans[bowler];
                    }
                }

                return acc;
            }, {});
            console.log(topTenEconomicalBaller);

            const ans1 = Object.entries(topTenEconomicalBaller).sort((prev, cur) => {
                if (prev[1] > cur[1]) {
                    return 1;
                }
                else if (prev[1] < cur[1]) {
                    return -1;
                }
                else {
                    return 0;
                }
            }).slice(0, 10);
            const obj = Object.fromEntries(ans1);
            res.json(obj);


        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });



}
module.exports = answer4;