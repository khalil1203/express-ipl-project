const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');



const answer5 = (req, res, next) => {


    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            const dataArr = data.reduce((acc, entries) => {
                const { winner, toss_winner } = entries;
                if (entries.winner === toss_winner) {
                    if (!acc[winner]) {
                        acc[winner] = 0;
                    }
                    acc[winner] += 1;
                }
                return acc;
            }, {});
            res.json(dataArr);
        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });

}
module.exports = answer5;
