const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');


const answer6 = (req, res, next) => {

    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            const ansData = data.reduce((result, playerData) => {
                const foundItem = result.find((item) => {

                    return item.season === playerData.season && item.player === playerData.player_of_match;
                });
                if (foundItem) {
                    foundItem.motm += 1;
                }
                else {
                    result.push({
                        season: playerData.season,
                        player: playerData.player_of_match,
                        motm: 1
                    });
                }
                return result;

            }, [])
                .sort((prev, cur) => {
                    if (prev.season > cur.season) {
                        return 1;
                    } else if (prev.season < cur.season) {
                        return -1;
                    } else {
                        return 0;
                    }

                })
                .reduce((result, curItem, index) => {

                    if (index === 0) {
                        result.push(curItem);
                        return result;
                    }

                    const prevItem = result[result.length - 1];

                    if (curItem.season !== prevItem.season) {
                        result.push({
                            season: curItem.season,
                            player: curItem.player,
                            motm: curItem.motm
                        });
                    }
                    else if (curItem.motm > prevItem.motm) {
                        prevItem.player = curItem.player;
                        prevItem.motm = curItem.motm;
                    }

                    return result;
                }, []);

            console.log(ansData);
            res.json(ansData);
        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });

}
module.exports = answer6;
