const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');
const csvToJson = require('csvtojson');





const answer7 = (req, res, next) => {

    const batsmen_Arr = [];
    const matchId_Arr = [];


    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            matchId_Arr.push(0);
            matchId_Arr.push(...data.map((item) => {
                return item.season;
            }));
        });

    csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveries) => {
            deliveries.map((delivery) => {
                const id = parseInt(delivery.match_id);
                const year = parseInt(matchId_Arr[id]);
                const curbatsmen = delivery.batsman;
                const curRuns = parseInt(delivery.batsman_runs);
                let found = false;

                batsmen_Arr.map((batsman) => {
                    if (batsman.season === year && batsman.player === curbatsmen) {
                        batsman.runs += curRuns;
                        batsman.ball += 1;
                        found = true;
                        batsman.strikeRate = (batsman.runs * 100) / batsman.ball;
                    }
                });

                if (!found) {
                    batsmen_Arr.push({
                        season: year,
                        player: curbatsmen,
                        runs: curRuns,
                        ball: 1,
                        strikeRate: curRuns * 100
                    });
                }
            });

            const ans = batsmen_Arr.sort((prev, cur) => {
                if (prev.season > cur.season) {
                    return 1;
                } else if (prev.season < cur.season) {
                    return -1;
                } else {
                    return 0;
                }
            });

            res.json(ans);
        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });

}

module.exports = answer7;
