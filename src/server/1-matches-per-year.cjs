const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

//fetching data from matches.csv file
const answer1 = (req, res, next) => {



    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {

            let matchesArr = data.reduce((acc, entries) => {
                if (acc[entries.season]) {
                    acc[entries.season] += 1;
                }
                else {
                    acc[entries.season] = 1;
                }
                return acc;
            }, {});

            res.json(matchesArr);
        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });
}



module.exports = answer1;