const deliveriesPath = 'src/data/deliveries.csv';

const fs = require('fs');
const csvToJson = require('csvtojson');



const answer8 = (req, res, next) => {

    const player_Arr = [];

    csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveries) => {
            deliveries.map((delivery) => {
                if (delivery.player_dismissed !== '') {
                    const foundIndex = player_Arr.find(
                        (player) => player.bowler === delivery.bowler && player.batsmen === delivery.batsman
                    );

                    if (foundIndex) {
                        foundIndex.count += 1;
                    } else {
                        player_Arr.push({
                            batsmen: delivery.batsman,
                            bowler: delivery.bowler,
                            count: 1
                        });
                    }
                }
            });

            const sortedArr = player_Arr.sort((prev, cur) => cur.count - prev.count);
            res.json(sortedArr);


        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });

}
module.exports = answer8;
