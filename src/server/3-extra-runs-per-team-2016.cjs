const csvToJson = require('csvtojson');
const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

const answer3 = (req, res, next) => {

    match_Id_Arr = [];
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            match_Id_Arr.push(
                ...data
                    .filter((item) => {
                        return item.season === '2016';
                    })
                    .map((item) => {
                        return item.id;
                    })
            );
        });


    csvToJson()
        .fromFile(deliveriesPath)
        .then((data) => {

            const extraRunsPerTeam = data.reduce((acc, entries) => {
                const { bowling_team, extra_runs } = entries;
                if (match_Id_Arr.includes(entries.match_id)) {

                    if (acc[bowling_team]) {
                        acc[bowling_team] += parseInt(extra_runs);
                    }
                    else {
                        acc[bowling_team] = parseInt(extra_runs);
                    }
                }
                return acc;
            }, {});

            res.json(extraRunsPerTeam);

        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });


}
module.exports = answer3;

