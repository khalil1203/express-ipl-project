const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

const answer2 = (req, res, next) => {

    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            const dataArr = data.reduce((acc, entries) => {
                const { winner, season } = entries;
                if (!acc[winner]) {
                    acc[winner] = {};
                }
                if (!acc[winner][season]) {
                    acc[winner][season] = 0;
                }
                acc[winner][season] += 1;
                return acc;
            }, {});
            res.json(dataArr);
        }).catch((err) => {
            next({
                message: "Cannot fetch !!!!"
            })
        });
}

module.exports = answer2;
